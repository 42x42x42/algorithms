// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a pyramid shape
// with N levels using the # character.  Make sure the
// pyramid has spaces on both the left *and* right hand sides
// --- Examples
//   pyramid(1)
//       '#'
//   pyramid(2)
//       ' # '
//       '###'
//   pyramid(3)
//       '  #  '
//       ' ### '
//       '#####'

function pyramid(n) {
    let sharp = 1;
    let length = sharp + 2 * n;
    let space = (length - sharp) / 2 - 1;

    for (let i = 1; i <= n; i++) {
        console.log(' '.repeat(space) + "#".repeat(sharp) + ' '.repeat(space));
        sharp += 2;
        space--;
    }
}

module.exports = pyramid;
