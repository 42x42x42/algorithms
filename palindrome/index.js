// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false

// OPTION 1 simple
function palindrome(str) {
    let reverse = str.split('').reverse().join('');
    return reverse === str;
}

// //OPTION 2 - not so efficient
// function palindrome(str) {
//     return str.split('').every((char, index) => (
//         char === str[str.length - index - 1]
//     ))
// }

module.exports = palindrome;
