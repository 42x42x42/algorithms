// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
    let str = '' + n;
    return parseInt(str.split('').reverse().join('')) * Math.sign(n); // math.sign return -1 if '-' and 1 if '+' and 0 if zero
}

module.exports = reverseInt;
