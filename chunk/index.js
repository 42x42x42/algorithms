// --- Directions
// Given an array and chunk size, divide the array into many subarrays
// where each subarray is of length size
// --- Examples
// chunk([1, 2, 3, 4], 2) --> [[ 1, 2], [3, 4]]
// chunk([1, 2, 3, 4, 5], 2) --> [[ 1, 2], [3, 4], [5]]
// chunk([1, 2, 3, 4, 5, 6, 7, 8], 3) --> [[ 1, 2, 3], [4, 5, 6], [7, 8]]
// chunk([1, 2, 3, 4, 5], 4) --> [[ 1, 2, 3, 4], [5]]
// chunk([1, 2, 3, 4, 5], 10) --> [[ 1, 2, 3, 4, 5]]

function chunk(array, size) {


    // let start = 0;
    // let chunk = [];
    // for (let end = size; start <= array.length - 1; end += size) {
    //     chunk.push(array.slice(start, end));
    //     start = start + size;
    // }

    // переписанная версия того же что и выше, убран end тк он не нужен и равен start+size
    let chunk = [];
    for (let start = 0; start <= array.length - 1; start += size) {
        chunk.push(array.slice(start, start + size));
    }
    return chunk
}

module.exports = chunk;
