// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

function maxChar(str) {
    const arr = str.split('');
    const set = new Set(arr);
    const map = new Map();
    set.forEach(el => {
        map.set(el, 0)
    });

    arr.forEach(el => {
        map.set(el, map.get(el) + 1)
    });

    let maxChar;
    let maxCharValue = 0;
    for (let [key, val] of map.entries()) {
        if (val > maxCharValue) {
            maxChar = key;
            maxCharValue = val;
        }
    }
    return maxChar
}


module.exports = maxChar;
