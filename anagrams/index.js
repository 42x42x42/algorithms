// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True
//   anagrams('Hi there', 'Bye there') --> False


// OPTION 2 отсортировать символы в строках и сравнить две отсортированные строки
function anagrams(stringA, stringB) {
    function formatString(str) {
        return str.toLowerCase().split('').filter(el => el.match(/^[A-Za-z]+$/)).sort().join('');
    }

    return formatString(stringA) === formatString(stringB);
}

module.exports = anagrams;

// // OPTION 1
// function anagrams(stringA, stringB) {
//
//     const letters = /^[A-Za-z]+$/;
//     // formatted chars without duplicates in Set
//     const formStrA = stringA.toLowerCase().split('').filter(el => el.match(letters));
//     const formStrB = stringB.toLowerCase().split('').filter(el => el.match(letters));
//
//     if (formStrA.length !== formStrB.length) return false;
//
//     // map to store count of char usage
//     const mapA = new Map();
//     // count usage of chars in a
//     formStrA.forEach(el => {
//         // if (!mapA.get(el)) {
//         //     mapA.set(el, 1);
//         // } else {
//         //     mapA.set(el, mapA.get(el) + 1)
//         // }
//
//         // shorter version of code above
//         mapA.set(el, mapA.get(el) + 1 || 1)
//
//     });
//
//     let result = true;
//     formStrB.forEach(el => {
//         if (!mapA.get(el)) {
//             result = false;
//         } else {
//             mapA.set(el, mapA.get(el) - 1);
//             if (mapA.get(el) < 0) result = false;
//         }
//     });
//     return result
// }