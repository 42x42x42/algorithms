// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

// DECISION 1
// function reverse(str) {
//     var reversed = '';
//     for (var i = str.length - 1; i >= 0; i--) {
//         reversed = reversed + str[i];
//     }
//     return reversed;
// }


// DECISION 2
// function reverse(str) {
// return str.split('').reverse().join('')
// }

// DECISION 3
function reverse(str) {
    // return str.split('').reduceRight((acc, char) => (acc + char));
    return str.split('').reduce((acc, char) => (char+acc),'');
}

module.exports = reverse;
