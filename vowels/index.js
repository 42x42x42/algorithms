// --- Directions
// Write a function that returns the number of vowels
// used in a string.  Vowels are the characters 'a', 'e'
// 'i', 'o', and 'u'.
// --- Examples
//   vowels('Hi There!') --> 3
//   vowels('Why do you ask?') --> 4
//   vowels('Why?') --> 0

// //Option 1
// function vowels(str) {
//     const vowels = 'aeiou';
//     let counter = 0;
//     str.toLowerCase().split('').forEach(char => {
//         vowels.includes(char) ? counter++ : char
//     });
//     return counter
// }

// //Option 2
function vowels(str) {
    const vowels = 'aeiou';
    return str.toLowerCase().split('').filter (char => (vowels.includes(char))).length;
}



// //Option 3: regexp
// function vowels(str) {
//     const matches = str.match(/[aeiou]/gi);
//     return matches ? matches.length : 0;
// }

// //Option 4: regexp
// function vowels(str) {
//     return str.replace(/[^euioa]/gi, '').length;
// }

module.exports = vowels;
